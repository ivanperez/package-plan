#!/usr/bin/perl

use strict;
use warnings;

use Dpkg::Version;
use File::Slurp;

my $packages = read_file('packages.txt');

my %lts;
print "Reading lts.config...\n";
open LTS, "<", "lts.config" or die $!;
while (<LTS>) {
     chomp;
     next if /^#/;
     next if /^--/;
     next if /^\s*$/;
     next if /installed,?$/;
     unless (m/^(?:constraints:)?\s+(.*?) ==(.*?),?$/) {
             print "Ignoring unparseable line $.: $_\n";
     }
     my ($pkg,$version) = ($1,$2);

     if ($packages =~ /^$pkg\s+([^\s]+)/m) {
	my $before = $1;
	if (version_compare($before, $version) == -1)  {
		$packages =~ s/^$pkg\s+[^\s]+/$pkg $version/m;
		$packages =~ s/\s*ahead//;
		printf "Bumped %s to %s\n", $pkg, $version;
	}
     }
}
close LTS;

write_file('packages.txt', $packages);

